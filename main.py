# import modul
from bs4 import BeautifulSoup
import requests
import csv

response =requests.get('https://www.futbin.com/20/players?page=1')

# paginasi + tambah data
datas = []
for page in range(1, 755):
    req = requests.get('https://www.futbin.com/20/players?page='+str(page))

soup = BeautifulSoup(response.text, 'html.parser')

numbers = [1,2]
for number in numbers:
    players = soup.findAll('tr', 'player_tr_'+str(number))

    for p in players:
        tds = p.findAll('td')
        name = tds[0].text.strip()
        clubs = p.find('span', 'players_club_nation').findAll('a')
        club = clubs[0]['data-original-title'].replace('Icons', 'unknown').strip()
        nation = clubs[1]['data-original-title'].replace('Icons', 'unknown').strip()
        league = clubs[2]['data-original-title'].replace('Icons', 'unknown').strip()
        
        print('Name: ', name)
        print('Club: ', club)
        print('Nation: ', nation)
        print('League', league, '\n')
        datas.append(
            [
                name,
                club,
                nation,
                league
            ]
        )

        with open('futbin.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            headers = [
                'Name',
                'Club',
                'Nation',
                'League',
            ]
            writer.writerow(headers)
            for data in datas:
                writer.writerow(data)